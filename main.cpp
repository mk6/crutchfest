#include <iostream>
#include <sstream>
#include <string>

using namespace std;

enum Type {frac, expr};

struct Fraction {
    long numerator = 0;
    long denominator = 1;

    void simplify();
    void show();
    void flip();
};

void Fraction::flip() {
    swap(this->numerator, this->denominator);
}

Fraction& operator+(Fraction &lhs, Fraction &rhs) {
    Fraction *res = new Fraction();

    res->numerator = lhs.numerator * rhs.denominator +
                    rhs.numerator * lhs.denominator;
    res->denominator = lhs.denominator * rhs.denominator;
    res->simplify();

    return *res;
}

Fraction& operator-(Fraction &lhs, Fraction &rhs) {
    Fraction *res = new Fraction();

    res->numerator = lhs.numerator * rhs.denominator -
                    rhs.numerator * lhs.denominator;
    res->denominator = lhs.denominator * rhs.denominator;
    res->simplify();

    return *res;
}

Fraction& operator*(Fraction &lhs, Fraction &rhs) {
    Fraction *res = new Fraction();

    res->numerator = lhs.numerator * rhs.numerator;
    res->denominator = lhs.denominator * rhs.denominator;
    res->simplify();

    return *res;
}

Fraction& operator*(Fraction &lhs, long multiplier) {
    Fraction *res = new Fraction();

    res->numerator = lhs.numerator * multiplier;
    res->simplify();

    return *res;
}

Fraction& operator/(Fraction &lhs, Fraction &rhs) {
    Fraction *res = new Fraction();

    res->numerator = lhs.numerator * rhs.denominator;
    res->denominator = lhs.denominator * rhs.numerator;
    res->simplify();

    return *res;
}

void Fraction::simplify() {
    long divisor, a = this->numerator, b = this->denominator;

    while (b != 0) {
        divisor = b;
        b = a % b;
        a = divisor;
    }

    this->numerator /= std::abs(a);
    this->denominator /= std::abs(a);

    if ((this->denominator < 0 && this->numerator < 0) || this->denominator < 0) {
        this->numerator *= -1;
        this->denominator *= -1;
    }
}

void Fraction::show() {
    std::cout << this->numerator << " / " << this->denominator << "\n";
}

bool operator==(Fraction& lhs, Fraction& rhs) {
    return lhs.numerator == rhs.numerator &&
        lhs.denominator == rhs.denominator;
}

bool operator!=(Fraction& lhs, Fraction& rhs) {
    return !(lhs == rhs);
}

struct Node {
    Type type;
    Fraction fraction;
    string data;
    Node *next = nullptr;
    string toString();
};

string Node::toString() {
    if (this->type == frac) {
        return to_string(this->fraction.numerator) +
               " " +
               to_string(this->fraction.denominator) +
               " / ";
    } else {
        return this->data;
    }
}

struct Stack {
    int height = 0;
    Node *top = nullptr;
    void push(std::string);
    void push(Fraction);
    void show();
    Node *pop();
};

void Stack::show() {
    Node *t = this->top;

    while (t != nullptr) {
        cout << t->toString() << " ";
        t = t->next;
    }
    cout << "\n";
}
void Stack::push(std::string s) {
    Node* t = new Node;
    t->type = expr;
    t->data = s;
    t->next = top;
    top = t;

    height++;
}

void Stack::push(Fraction f) {
    Node *t = new Node;
    t->fraction = f;
    t->next = this->top;
    top = t;

    height++;
}

Node* Stack::pop() {
    if (this->top == nullptr) {
        return nullptr;
    }
    Node *t = this->top;
    this->top = this->top->next;
    t->next = nullptr;
    height--;

    return t;
}

void eval(Fraction &a, Fraction &b, Fraction &res, string &op, bool &divZero) {
    if (op == "+") {
        res = a + b;
    } else if (op == "-") {
        res = a - b;
    } else if (op == "*") {
        res = a * b;
    } else {
        if (b.numerator == 0) {
            cout << "Ошибка: деление на 0 в выражении.\n";
            divZero = true;
            return;
        }
        res = a / b;
    }
}

void evalR(Fraction &rightRes, Fraction &a, string &op, bool &divZero, bool &mulZero) {
    if (op == "+") {
        rightRes = rightRes - a;
    } else if (op == "-") {
        rightRes = rightRes + a;
    } else if (op == "*") {
        if (a.numerator == 0) {
            mulZero = true;
            rightRes.numerator = 0;
            rightRes.denominator = 1;
            return;
        }
        rightRes = rightRes / a;
    } else {
        if (a.numerator == 0) {
            /*
            cout << "Ошибка: деление на 0 в выражении.\n";
            divZero = true;
            return;
            */
        }
        rightRes = rightRes * a;
    }
}

void Solve(string equation) {
    if (equation.size() > 80) {
        cout << "Превышен допустимый размер выражения\n";
        return;
    }

    bool foundX = false;
    bool mulZero = false;
    bool divZero = false;
    bool xInDen = false;

    stringstream ss;
    ss.str(equation);
    string tmp;

    Stack leftPart;
    Fraction rightPart;


    while(ss >> tmp) {
        if (tmp == "x" || tmp == "X") {
            if (!foundX) {
                leftPart.push(tmp);
                foundX = true;
            } else {
                cout << "Ошибка: несколько X в выражении\n";
                return;
            }
        } else if (tmp == "+" || tmp == "-" || tmp == "*" || tmp == "/") {
            Node* right = leftPart.top;
            leftPart.pop();
            Node* left = leftPart.top;
            leftPart.pop();

            if (left == nullptr || right == nullptr) {
                cout << "Ошибка: не совпадают операторы и операнды\n";
                delete right;
                delete left;
                return;
            }

            if (left->type == expr || right->type == expr) {
                if (ss.peek() == EOF) {
                    ss.clear();
                    foundX = false;
                    if (left->type == expr) {
                        if (tmp == "/" && right->fraction.numerator == 0) {
                            cout << "Ошибка: деление на ноль в выражении\n";
                            return;
                        }
                        evalR(rightPart, right->fraction, tmp, divZero, mulZero);
                        ss.str(left->toString());
                    } else {
                        if (tmp == "-") {
                            rightPart = rightPart * -1;
                        } else if (tmp == "/") {
                            if (left->fraction != rightPart && rightPart.numerator == 0 && !mulZero) {
                                cout << "Нет решений\n";
                                return;
                            }
                            xInDen = true;
                            if (rightPart.numerator != 0)
                                rightPart.flip();
                        }
                        evalR(rightPart, left->fraction, tmp, divZero, mulZero);
                        ss.str(right->toString());
                    }
                    while (leftPart.top != nullptr) {
                        Node *x = leftPart.pop();
                        delete x;
                    }
                } else {
                    leftPart.push(left->toString() +
                                  " " +
                                  right->toString() +
                                  " " +
                                  tmp);
                }
            } else {
                Fraction c;
                eval(left->fraction, right->fraction, c, tmp, divZero);
                if (divZero) {
                    return;
                }
                leftPart.push(c);
            }
        } else {
            try {
                Fraction c;
                c.numerator = stol(tmp);
                leftPart.push(c);
            } catch (const std::exception& ex) {
                cout << "Ошибка: недопустимые символы в выражении\n";
                return;
                //std::cerr << ex.what() << "\n";
            }
        }
    }

    //leftPart.show();
    if (leftPart.height > 1) {
        cout << "Ошибка: недостаточно операторов\n";
    } else if (leftPart.height == 1 && !divZero) {
        if (leftPart.top->type == expr) {
            if (mulZero && !xInDen) {
                cout << "X - любой\n";
                return;
            }

            if (mulZero && xInDen) {
                cout << "X - любой, кроме ";
                rightPart.show();
                return;
            }
            if (mulZero == false && !xInDen) {
                cout << "X = ";
            } else if (xInDen == true) {
                cout << "X - любое, кроме ";
            }
            rightPart.show();
        } else {
            if (leftPart.top->fraction == rightPart) {
                cout << "X - любой\n";
                ///
            } else {
                cout << "Нет решений\n";
            }
        }
    }
}

int main() {
    std::string equation;
    stringstream resStream;

    do {
        cout << "> ";
        getline(cin, equation);
        //boost::algorithm::trim_right_copy(equation);
        Solve(equation);
    } while(equation != "");
    cout << "До свидания!" << endl;

    return 0;
}
